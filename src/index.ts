import { writeToPath } from '@fast-csv/format';
import path from 'path';
import { Command } from 'commander';


const program = new Command();

program.version('0.0.1');

program
    .option('-d, --debug', 'output extra debugging', false)
    .option('-f, --file-name <string>', 'set output\'s CSV file with name `<string>.csv`', 'data')
    .option('-l, --length <integer>', 'set number of leaf nodes', '5000')
    .option('-s, --skew <float>', 'set skew of distribution (1.0 is no skew; higher is closer to root)', "1.0")
    .option('-r, --range <float>', 'set range as percentage of tree history to grow backwards from (0.0 to 1.0)', "1.0")
    .option('-o, --offset <integer>', 'set root node ID integer ', "0")
    ;

program.parse(process.argv);

const options = program.opts();

const FILE_NAME: string = options.fileName || 'data';
const LENGTH = parseInt(options.length, 10) || 500;
const SKEW = parseFloat(options.skew) || 1.0;
const OFFSET = parseInt(options.offset, 10) || 0;
const RANGE = parseFloat(options.range) || 1.0;

if (options.debug) {
    console.log({ FILE_NAME, LENGTH, SKEW, OFFSET });
}


function randn_bm(min: number, max: number, skew: number) {
    let u = 0, v = 0;
    while (u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while (v === 0) v = Math.random();
    let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return num;
}

const round_to_precision = (x: number, precision: number) => {
    // from https://stackoverflow.com/a/9280882
    return Math.floor(x / precision) * precision;
}


const counts: { [key: number]: number; } = {};
counts[OFFSET] = 0;

let single: number | null = null;

const rows: (number | string)[][] =
    (new Array(LENGTH)).fill(null)
        .map((_el, i) => i + 1)
        .map(i => {

            let toId: number | null = null;

            if (single === null) {
                const upper = OFFSET + i;
                const lower = upper - (upper - OFFSET) * RANGE;

                toId = round_to_precision(randn_bm(lower, upper, SKEW), 1);
            } else {
                toId = single;
            }

            if (counts[toId] === undefined) {
                counts[toId] = 0;
            }
            counts[toId] = counts[toId] + 1;
            if (counts[toId] === 1) {
                single = toId;
            } else {
                single = null;
            }

            return [i, toId];
        });

rows.unshift(["from", "to"]);

const PATH_NAME = path.resolve(__dirname, `${FILE_NAME}.csv`);

writeToPath(PATH_NAME, rows)
    .on('error', err => console.error(err))
    .on('finish', () => console.log(`Done writing ${PATH_NAME}`));