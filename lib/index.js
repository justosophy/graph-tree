"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var format_1 = require("@fast-csv/format");
var path_1 = __importDefault(require("path"));
var commander_1 = require("commander");
var program = new commander_1.Command();
program.version('0.0.1');
program
    .option('-d, --debug', 'output extra debugging', false)
    .option('-f, --file-name <string>', 'set output\'s CSV file with name `<string>.csv`', 'data')
    .option('-l, --length <integer>', 'set number of leaf nodes', '5000')
    .option('-s, --skew <float>', 'set skew of distribution (1.0 is no skew; higher is closer to root)', "1.0")
    .option('-r, --range <float>', 'set range as percentage of tree history to grow backwards from (0.0 to 1.0)', "1.0")
    .option('-o, --offset <integer>', 'set root node ID integer ', "0");
program.parse(process.argv);
var options = program.opts();
var FILE_NAME = options.fileName || 'data';
var LENGTH = parseInt(options.length, 10) || 500;
var SKEW = parseFloat(options.skew) || 1.0;
var OFFSET = parseInt(options.offset, 10) || 0;
var RANGE = parseFloat(options.range) || 1.0;
if (options.debug) {
    console.log({ FILE_NAME: FILE_NAME, LENGTH: LENGTH, SKEW: SKEW, OFFSET: OFFSET });
}
function randn_bm(min, max, skew) {
    var u = 0, v = 0;
    while (u === 0)
        u = Math.random(); //Converting [0,1) to (0,1)
    while (v === 0)
        v = Math.random();
    var num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0)
        num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return num;
}
var round_to_precision = function (x, precision) {
    // from https://stackoverflow.com/a/9280882
    return Math.floor(x / precision) * precision;
};
var counts = {};
counts[OFFSET] = 0;
var single = null;
var rows = (new Array(LENGTH)).fill(null)
    .map(function (_el, i) { return i + 1; })
    .map(function (i) {
    var toId = null;
    if (single === null) {
        var upper = OFFSET + i;
        var lower = upper - (upper - OFFSET) * RANGE;
        toId = round_to_precision(randn_bm(lower, upper, SKEW), 1);
    }
    else {
        toId = single;
    }
    if (counts[toId] === undefined) {
        counts[toId] = 0;
    }
    counts[toId] = counts[toId] + 1;
    if (counts[toId] === 1) {
        single = toId;
    }
    else {
        single = null;
    }
    return [i, toId];
});
rows.unshift(["from", "to"]);
var PATH_NAME = path_1.default.resolve(__dirname, "".concat(FILE_NAME, ".csv"));
(0, format_1.writeToPath)(PATH_NAME, rows)
    .on('error', function (err) { return console.error(err); })
    .on('finish', function () { return console.log("Done writing ".concat(PATH_NAME)); });
